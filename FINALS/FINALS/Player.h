#pragma once
#include <string>
#include "Pokemon.h"

using namespace std;

class Players
{
	public:
		Players();
		Players(string name);
		string name;
		Pokemons* pokemon;
		int x = 0;
		int y = 0;

		string locationName;

		void displayStats();
		void location(int x, int y);
		void mainAction();
		void encounterPokemon();

};

