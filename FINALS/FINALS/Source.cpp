#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <vector>
#include "Player.h"
#include "Pokemon.h"

using namespace std;

int main()
{
	srand(time(NULL));
	string name;
	int choice;
	

	//START
	cout << "Hi, I am Professor Oak.." << endl;
	_getch();
	cout << "Professor Oaks want to know your name.." << endl;
	cout << "Your name is: ";
	cin >> name;
	
	system("pause");
	system("cls");

	cout << "Welcome to the World of Pokemon" << endl;
	_getch();
	cout << "I have three(3) Pokemons here" << endl;
	cout << "You can have one! Go on, choose!" << endl;
	cout << "[1] - Bulbasaur" << endl;
	cout << "[2] - Charmander" << endl;
	cout << "[3] - Squirtle" << endl;
	cout << endl;
	cout << "Your choice: ";
	cin >> choice;

	Pokemons* pokemon[15] = {new Pokemons("Bulbasaur", 59, 59, 59, 5, 19), new Pokemons("Charmander", 59, 59, 59, 5, 19), new Pokemons("Squirtle", 59, 59, 59, 5, 19), new Pokemons("Pidgey", 61, 61, 61, 5, 15), new Pokemons("Rattata", 55, 55, 55, 6, 18), new Pokemons("Pikachu", 65, 65, 65, 7, 25), new Pokemons("jigglypuff", 57, 57, 57, 5, 17), new Pokemons("zubat", 59, 59, 59, 5, 15), new Pokemons("psyduck", 45, 45, 45, 5, 15), new Pokemons("abra", 50, 50, 50, 5, 15), new Pokemons("geodude", 50, 50, 50, 5, 17 ), new Pokemons("magikarp", 50, 50, 50, 5, 19), new Pokemons("gyarados", 56, 56, 56, 7, 19), new Pokemons("snorlax", 58, 58, 58, 7, 18), new Pokemons("Togepi", 60, 60, 60, 8, 20)};
	
	Players* player = new Players(name);
	if (choice == 1 )
	{
		cout << "You chose Balbasaur!" << endl;	 
		player->pokemon = pokemon[0];
	}
	else if (choice == 2)
	{
		cout << "You chose Charmander!" << endl;
		player->pokemon = pokemon[1];
	}
	else if (choice == 3)
	{
		cout << "You chose Squirtle!" << endl;
		player->pokemon = pokemon[2];
	}
	system("pause");
	system("cls");

	cout << "Your journey begins now!" << endl;
	system("pause");
	system("cls");

	player->mainAction();

	


	system("pause");
	return 0;
}