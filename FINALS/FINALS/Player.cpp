#include <iostream>
#include <string>
#include "Player.h"
#include "Pokemon.h"

using namespace std;

Players::Players()
{
	this->name = "";
	this->x = 0;
	this->y = 0;

}

Players::Players(string name)
{
	this->name = name;
	this->pokemon = pokemon;
	this->x = 0;
	this->y = 0;

}

void Players::location(int x, int y)
{
	char move;

	if (x >= -2 && x <= 2 && y >= -2 && y <= 2)
	{
		locationName = "Pallet Town";
	}
	else
	{
		locationName = "Unknown Area";
	}

	cout << "Where do you want to move?" << endl << endl;
	cout << "Location: " << locationName << endl << endl;
	cout << "[W] - Up     [S] - Down     [A] - Left     [D] - Right" << endl << endl;
	cout << "Move: ";
	cin >> move;

	if (move == 'W' || move == 'w')
	{
		this->y++;
		cout << "You are now at position " << "(" << this->x << "," << this->y << ")" << endl;
	}
	if (move == 'S' || move == 's')
	{
		this->y--;
		cout << "You are now at position " << "(" << this->x << "," << this->y << ")" << endl;
	}
	if (move == 'A' || move == 'a')
	{
		this->x--;
		cout << "You are now at position " << "(" << this->x << "," << this->y << ")" << endl;
	}
	if (move == 'D' || move == 'd')
	{
		this->x++;
		cout << "You are now at position " << "(" << this->x << "," << this->y << ")" << endl;
	}

	system("Pause");
	system("cls");
}

void Players::mainAction()
{
	while (pokemon != 0)
	{
		int choice;
		cout << "What would you like to do?" << endl;
		cout << "[1] - Move" << endl;
		cout << "[2] - Pokemon" << endl;
		if (x >= -2 && x <= 2 && y >= -2 && y <= 2)
		{
			cout << "[3] - Pokemon Center" << endl;
		}
			cin >> choice;
			system("cls");
		
		if (choice == 1)
		{
			location(x, y);
		}
		else if (choice == 2)
		{
			pokemon->displayStats(pokemon);
		}
		else if (choice == 3)
		{
			pokemon->pokemonCenter();
		}
	}
}

void Players::encounterPokemon()
{
	int choice;
	cout << "You have encountered a wild " << pokemon->pokemonRandom;
	this->pokemon->pokemonRandom.displayStats;
	cout << endl << endl;
	system("Pause");
	cout << endl << endl;
	cout << "What would you like to do? " << endl;
	cout << "[1] - Battle     [2] - Catch     [3] - Run Away" << endl;
	cin >> choice;

	if (choice == 1)
	{
		pokemon->pokemonRandom();

	}			
	else if (choice == 2)
	{


	}
	else if (choice == 3)
	{
		cout << "You ran away safely....." << endl;
	}

}

void Players::displayStats()
{

	cout << "Player " << this->name << " stats" << endl;
	cout << "Location: " << "(" << this->x << "," << this->y << ")" << endl;

}
